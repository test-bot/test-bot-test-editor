/**
 * Listens for the app launching then creates the window
 *
 * @see http://developer.chrome.com/apps/app.runtime.html
 * @see http://developer.chrome.com/apps/app.window.html
 */
chrome.app.runtime.onLaunched.addListener(function(launchData) {
  chrome.app.window.create('mainpage.html',
    {
    	id: "mainwin",
    	innerBounds: {width: 1000, height: 500}
    }, function (win) {
        // win.contentWindow.onload = function () {
        //     var wv = this.document.querySelector('webview');
        //       wv.addEventListener('contentload', function(e) {
        //         wv.executeScript({ code: "document.querySelector('.mk-li-paper-plane').style.backgroundColor='red'" });
        //       });
        // }
        // 
        if (launchData.source === 'url_handler') {
            var currentPath = getLocation(launchData.url).pathname;
            if (currentPath.indexOf('/test-editor') === 0) {
                currentPath = currentPath.split('/test-editor')[1];
            }
            win.contentWindow.uaPath = currentPath;
        }
    });
});

function getLocation(path) {
    var l = document.createElement("a");
    l.href = path;
    return l;
}

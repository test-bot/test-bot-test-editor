module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks("grunt-systemjs-builder");
 
    var appDependencies = [
        "node_modules/core-js/client/shim.min.js",
        "node_modules/zone.js/dist/zone.js",
        "node_modules/reflect-metadata/Reflect.js",
        "node_modules/angular/angular.min.js",
        "node_modules/@angular/router/angular1/angular_1_router.js"
    ];

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        uglify: {
            options: {
                mangle: false
            },
            default: {
                options: {
                    mangle: false,
                    compress: false,
                    beautify: true,
                    preserveComments: false
                },
                files: [{'build/dependencies.js': appDependencies}]
            }
        },

        systemjs: {
            options: {
                //baseURL: 'node_modules',
                sfx:true,
                configFile: 'systemjs.config.js'
            },
            dist: {
                files: [{
                    "src":  'target/boot.js',
                    "dest": "build/bundle.js"
                }]
            }
        },

        html2js: {
          options: {
            module: 'templates',
            base: 'app'
          },
          main: {
            singleModule: true,
            src: ['app/templates/*.html', 'app/templates/**/*.html'],
            dest: 'build/templates.js'
          },
        },

        ts: {
            default: {
                tsconfig: true,
                options: {
                    "compiler": './node_modules/typescript/bin/tsc',
                }
            }
        },
        watch: {
            files: ['app/scripts/**/*.ts'],
            tasks: ['uglify', 'ts', 'html2js', 'systemjs']
        }
    });
 
    grunt.registerTask('default', ['uglify', 'ts', 'html2js', 'systemjs']);
    grunt.registerTask('dev', ['watch']);
}
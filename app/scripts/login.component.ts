import {Utils} from './utils';
import {uaModule} from './module';

export class LoginComponent {
    $router: ng.Router;

    constructor() {

    }

    $onInit() {
        
    }

    loginLinkedIn() {
        var url = 'http://localhost:3000' + '/auth/app/linkedin?appId=' + chrome.runtime.id;
        var that = this;

        chrome.identity.launchWebAuthFlow({ url: url, interactive: true }, (callbackUrl) => {
            var accessToken = Utils.getParameterByName('accessToken', callbackUrl);
            if (accessToken) {
                chrome.storage.local.set({ usabiliticsAccessToken: accessToken });
                that.$router.navigate(['Home']);
            } else {
                that.loginMessage ='Invalid login.';
            }
        });
    }

    loginMessage: String;
}

uaModule
    .component('loginComponent', {
        templateUrl: 'app/templates/login.component.html',
        bindings: { $router: '<' },
        controller: LoginComponent
    });
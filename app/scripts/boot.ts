// import {AppComponent}     from './app.component';
// import {bootstrapStatic}        from '@angular/platform-browser';
// import {ROUTER_PROVIDERS} from '@angular/router-deprecated';
// import {LocationStrategy, Location, HashLocationStrategy } from '@angular/common';
// import {HTTP_PROVIDERS} from '@angular/http';
// import {provide} from '@angular/core';

// declare var appConfig: any;

// bootstrapStatic(AppComponent, [
//     ROUTER_PROVIDERS,
//     HTTP_PROVIDERS,
//     provide(LocationStrategy, { useClass: HashLocationStrategy })
// ]);
//

import { UpgradeAdapter } from '@angular/upgrade';
import { Http, ConnectionBackend, RequestOptions, HTTP_PROVIDERS } from '@angular/http';
import { ResponseOptionsArgs } from '@angular/http/src/interfaces';
import {uaModule} from './module';

import {AppComponent} from './app.component';
import {AuthenticationService} from './authentication-service';
import {ScenariosDataService} from './scenarios/scenarios-data-service';
import {ScenarioStore} from './scenarios/scenario-store'

AppComponent.name;

const upgradeAdapter = new UpgradeAdapter();

// upgradeAdapter.addProvider(ConnectionBackend);
// upgradeAdapter.addProvider();
// upgradeAdapter.addProvider(RequestOptions);
// upgradeAdapter.addProvider(Http);
upgradeAdapter.addProvider(HTTP_PROVIDERS);
upgradeAdapter.addProvider(AuthenticationService);
upgradeAdapter.addProvider(ScenariosDataService);
upgradeAdapter.addProvider(ScenarioStore);

uaModule
    .factory('authenticationService', upgradeAdapter.downgradeNg2Provider(AuthenticationService))
    .factory('scenariosService', upgradeAdapter.downgradeNg2Provider(ScenariosDataService))
    .factory('scenarioStore', upgradeAdapter.downgradeNg2Provider(ScenarioStore))
    .value('$routerRootComponent', 'uaApp');

setTimeout(() => {
    upgradeAdapter.bootstrap(document.querySelector('#uaApp'), ['uaAppModule'], { strictDi: true });
}, 5000);


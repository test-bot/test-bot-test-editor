import {uaModule} from './module';
import {LoginComponent} from './login.component';
import {HomeComponent} from './home.component';
import {ScenarioComponent} from './scenarios/scenario.component';

LoginComponent.name;
HomeComponent.name;
ScenarioComponent.name;

export class AppComponent {
    
}

uaModule.component('uaApp', {
    templateUrl: 'app/templates/app.component.html',
    controller: AppComponent,
    $routeConfig: [
        { path: '/login', name: 'Login', component: 'loginComponent' },
        { path: '/apps', name: 'Home', component: 'homeComponent', useAsDefault: true },
        { path: '/apps/:appId/scenarios/:scenarioId/...', name: 'Scenario', component: 'scenarioComponent'}
    ]
})
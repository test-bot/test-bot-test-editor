export enum ObjectState {
    Pending,
    Processing,
    Ready,
    Deleted
}
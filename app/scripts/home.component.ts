import {uaModule} from './module';

export class HomeComponent {
    $router: ng.Router;

    constructor() {

    }

    logOut () {
        chrome.storage.local.set({ usabiliticsAccessToken: null });
        this.$router.navigate(['Login']);
    }

    $onInit() {
        chrome.storage.local.get('usabiliticsAccessToken', (key) => {
            if (key['usabiliticsAccessToken']) {
                if(window['uaPath']) {
                    this.$router.navigateByUrl(window['uaPath']);
                }
            } else {
                //this.$location.path('/login');
                this.$router.navigate(['Login']);
            }
        });
    }
}

uaModule
    .component('homeComponent', {
        templateUrl: 'app/templates/home.component.html',
        bindings: { $router: '<' },
        controller: HomeComponent
    });
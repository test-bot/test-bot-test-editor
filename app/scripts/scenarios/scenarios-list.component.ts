import {ScenariosDataService} from './scenarios-data-service';
import {Scenario} from './scenario';

export class ScenariosListComponent {
    
    static $inject = ['scenariosService'];
    constructor(scenariosService: ScenariosDataService) {
        
    }

    $onInit () {

    }

    public scenarios: Scenario[];
}
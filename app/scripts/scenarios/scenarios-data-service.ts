import {Injectable, Inject} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switch';
import 'rxjs/add/observable/fromPromise';

import {AuthenticationService} from '../authentication-service';

@Injectable()
export class ScenariosDataService {
    constructor(private http: Http, private authService: AuthenticationService) {
        this.endpoint = 'http://localhost:8081/v1' + '/apps/';
    }

    public get(appId) {
        return Observable.fromPromise(this.authService.getAuthorizationHeader())
            .map((headers: Headers) => {
                return this.http.get(this.endpoint + appId + '/scenarios', {
                    headers: headers
                })
                    .map(res => res.json().items);
            }).switch();
        
    }

    public getOne(appId, scenarioId) {
        return Observable.fromPromise(this.authService.getAuthorizationHeader())
            .map((headers: Headers) => {

                return this.http.get(this.endpoint + appId + '/scenarios/' + scenarioId, {
                    headers: headers
                })
                    .map(res => res.json());
            }).switch();
    }

    public delete(appId: String, scenarioId: String) {
        return Observable.fromPromise(this.authService.getAuthorizationHeader())
            .map((headers: Headers) => {
                headers.append('Content-Type', 'application/json');

                return this.http.delete(this.endpoint + appId + '/scenarios/' + scenarioId, {
                    headers: headers
                });
            }).switch();
    }

    public ignore(appId: String, scenarioId: String, ignored: boolean) {
        return Observable.fromPromise(this.authService.getAuthorizationHeader())
            .map((headers: Headers) => {
                headers.append('Content-Type', 'application/json');

                return this.http.put(this.endpoint + appId + '/scenarios/' + scenarioId + '/ignore', JSON.stringify({ ignored: ignored }), {
                    headers: headers
                });
            }).switch();
    }

    private endpoint;
}
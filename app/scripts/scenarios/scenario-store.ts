import {Injectable} from '@angular/core';
import {Scenario} from './scenario';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class ScenarioStore {
    
    constructor () {
        this.currentScenario = new BehaviorSubject(null);
    }

    public currentScenario: BehaviorSubject<Scenario>;
}
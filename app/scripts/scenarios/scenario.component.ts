import {uaModule} from '../module';
import {ScenariosDataService} from './scenarios-data-service';
import {StepComponent} from './steps/step.component';
import {EditStepComponent} from './steps/edit-step.component';
import {ScenarioStore} from './scenario-store';

import {Scenario} from './scenario';
import {Step} from './steps/step';
import {WebviewComponent} from './steps/webview.component';

StepComponent.name;
EditStepComponent.name;
WebviewComponent.name;

export class ScenarioComponent {
    static $inject = ['scenariosService', 'scenarioStore'];
    constructor(private scenariosService: ScenariosDataService, private scenarioStore: ScenarioStore) {
        
    }

    home () {
        this.$router.navigate(['Home']);
    }

    viewStep(step: Step) {
        this.$router.navigate(['Step', {stepId: step.stepId}]);
    }

    $routerOnActivate (next, previous) {
        // Get the hero identified by the route parameter
        var appId = next.params.appId;
        var scenarioId = next.params.scenarioId;
        var that = this;
        return this.scenariosService.getOne(appId, scenarioId)
            .subscribe(s => {
                that.scenario = s;
                that.scenarioStore.currentScenario.next(s);
            });
    }

    $router: ng.Router;
    scenario: Scenario;
}

uaModule.component('scenarioComponent', {
    templateUrl: 'app/templates/scenarios/scenario.component.html',
    controller: ScenarioComponent,
    bindings: { $router: '<' },
    $routeConfig: [
        { path: '/steps/:stepId', name: 'Step', component: 'stepComponent'},
        { path: '/steps/:stepId/edit', name: 'EditStep', component: 'editStepComponent' }
    ]
});
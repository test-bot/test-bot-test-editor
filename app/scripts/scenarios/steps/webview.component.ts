import {uaModule} from '../../module';
import {Observable} from 'rxjs/Observable';
import {Step} from './Step';

export class WebviewComponent {
    
    constructor() {
        
    }

    $onInit() {
        this.webview = document.querySelector('#webview');

        this.step.subscribe(step => {
            if(step) {
                this.currentStep = step;
                if (this.webview.src !== step.pageUrl) {
                    this.webview.src = step.pageUrl;
                }
            }
        });
    }

    step: Observable<Step>;
    currentStep: Step;
    webview: any;
}

uaModule.component('webviewComponent', {
    templateUrl: 'app/templates/scenarios/steps/webview.component.html',
    bindings: { step: '<' },
    controller: WebviewComponent
});
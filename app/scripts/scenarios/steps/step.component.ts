import {uaModule} from '../../module';
import {ScenarioStore} from '../scenario-store';
import {Scenario} from '../scenario';

import {Step} from './step';
import {Subscription} from 'rxjs';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

export class StepComponent {
    static $inject = ['scenarioStore'];
    constructor(private scenarioStore: ScenarioStore) {
        this.stepSubject = new BehaviorSubject(null);
    }

    $routerOnActivate(next, previous) {
        // Get the hero identified by the route parameter
        var stepId = next.params.stepId;
        var scenarioId = next.params.scenarioId;
        
        this.scenarioSubscription = this.scenarioStore.currentScenario.subscribe(scenario => {
            if(scenario) {
                this.step = this.getStep(scenario, stepId);
                this.stepSubject.next(this.step);
            }
        });
    }

    $onDestroy () {
        if(this.scenarioSubscription) {
            this.scenarioSubscription.unsubscribe();
        }
    }

    private getStep(scenario: Scenario, stepId: String) {
        var filteredStep = scenario.steps.filter((step) => {
            return step.stepId === stepId;
        });

        if (filteredStep.length > 0) {
            return filteredStep[0];
        }
    }

    step: Step;
    stepSubject: BehaviorSubject<Step>;
    scenarioSubscription: Subscription;
}

uaModule.component('stepComponent', {
    templateUrl: 'app/templates/scenarios/steps/step.component.html',
    controller: StepComponent
});

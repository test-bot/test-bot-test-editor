import {uaModule} from '../../module';

export class EditStepComponent {
    
    constructor() {
        
    }
}

uaModule.component('editStepComponent', {
    templateUrl: 'app/templates/scenarios/steps/edit-step.component.html',
    controller: EditStepComponent
});
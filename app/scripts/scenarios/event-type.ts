export enum EventType {
    Click,
    Visited,
    Focus,
    EnterPressed
}
import {Step} from './steps/step';
import {ObjectState} from '../object-state';

export class Scenario {
    
    constructor() {
        // code...
    }

    public steps: Step[];
    public scenarioId: String;
    public appId: String;
    public name: String;
    public state: ObjectState;
    public ignored: boolean;
}
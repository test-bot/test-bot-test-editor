import {Headers} from '@angular/http';

export class AuthenticationService {
    constructor() {
    }

    getAuthorizationHeader() : Promise<Headers> {
        return new Promise((resolve, reject) => {

            chrome.storage.local.get('usabiliticsAccessToken', (keys) => {
                var token = keys['usabiliticsAccessToken'];
                if(token) {
                    resolve(new Headers({ Authorization: 'Bearer ' +  token}));
                } else {
                    reject('No access token.');
                }
            });
        });
    }
}